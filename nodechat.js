// Setup and create server
var https = require('https');
var fs = require('fs');

var httpOptions =  {
 key: fs.readFileSync(__dirname + '/keys/privatekey.pem'),
 cert: fs.readFileSync(__dirname + '/keys/certificate.pem')
}

var app = https.createServer(httpOptions, response);
var io = require('socket.io')(app);


// Create an image dictionary to translate tokens into images
var imagedict = {};
imagedict[1] = {text: ":thumbsup:", src: "/img/thumbsup.png"};
imagedict[2] = {text: ":smile:", src: "/img/smile.png"};
imagedict[3] = {text: ":wink:", src: "/img/wink.png"};
imagedict[4] = {text: ":grin:", src: "/img/grin.png"};
imagedict[5] = {text: ":laugh:", src: "/img/laugh.png"};
imagedict[6] = {text: ":angry:", src: "/img/angry.png"};
imagedict[7] = {text: ":shocked:", src: "/img/shocked.png"};
imagedict[8] = {text: ":omg:", src: "/img/omg.png"};
imagedict[9] = {text: ":sick:", src: "/img/sick.png"};
imagedict[10] = {text: ":poo:", src: "/img/poo.png"};
imagedict[11] = {text: ":tongue:", src: "/img/tongue.png"};
imagedict[12] = {text: ":noemotion:", src: "/img/noemotion.png"};


// Setup a blacklist which will ban certain IP addresses from connecting
// Can easily edit here or modify this to read from a file
var blacklist =
[
    "1.2.3.4",
    "5.6.7.8"
];


// Create a file stream for logging
var streamLog = fs.createWriteStream(getLogFilename(), {flags:'a'});


// Create a message history log which will cache the last maxMsgLogLength messages
var msgLog = [];
var maxMsgLogLength = 15;



app.listen(3000, "0.0.0.0");	// Listen on the specified port. The second param of 0.0.0.0 listens only on ipv4


console.log("Server running...");


// response() - Called when the server receives a connection requesting a page
function response(req, res) {
	var file = "";

	// Check the connection's IP address to see if it is blacklisted
    for(let i = 0; i < blacklist.length; i++) {
    	if(blacklist[i] == req.connection.remoteAddress) { // Rejection the connection
    		output = "Refused connection from " + blacklist[i];
        	console.log(output);   
			streamLog.write(output + "\r\n");        	
        	res.end(); 		
    	}
	}	

    
	// Disallow root directory
	if(req.url == "/") {
		output = "Attempted connection from " + req.connection.remoteAddress + " (req.url = \"/\")";
		console.log(output);
		streamLog.write(output + "\r\n");

		res.writeHead(404);
		return res.end('Page or file not found');
	} 
	else {
		file = __dirname + req.url;
	}
   
   	// Attempt to access the desired file path
	fs.readFile(file,
	    function (err, data) {
	    	// File not found
			if (err) {
				output = "Attempted connection from " + req.connection.remoteAddress + " (req.url = \"" + file + "\")";
				console.log(output);
				streamLog.write(output + "\r\n");

				res.writeHead(404);
				return res.end('Page or file not found');
			}

			// File was found, send to client
			res.writeHead(200);
			res.end(data);
	    }
    	);
}



// io.on("connection") - socket.io event when a client connects
io.on("connection", function(socket)
{
	// Send message history to client
    for(let i = 0; i < msgLog.length; i++) {
    	output = msgLog[i];
		io.to(socket.id).emit("serverSendMessage", output);    	
    }
	

	output = "Client connected (" + socket.request.connection.remoteAddress + ")";
	console.log(output);
	io.sockets.emit("serverSendMessage", output);
	streamLog.write(output + "\r\n");
	updateMessageHistory(output);			

	// socket.on("clientSendMessage") - custom socket.io event for when the client sends a standard message
   	socket.on("clientSendMessage", 
		function(sent_msg, callback) {
			output = "[ " + getCurrentDate() + " ]: " + "<" + sent_msg.username + "> " + sent_msg.msg;
			io.sockets.emit("serverSendMessage", output);
			streamLog.write(output + "\r\n");
			updateMessageHistory(output);
			callback();
	    }
	);

	// socket.on("clientSendEmote") - custom socket.io event for when the client sends an emote message
   	socket.on("clientSendEmote", 
		function(sent_msg, callback) {
			output = "[ " + getCurrentDate() + " ]: * " + sent_msg.username + " " + sent_msg.msg;
			io.sockets.emit("serverSendMessage", output);
			streamLog.write(output + "\r\n");
			updateMessageHistory(output);			
			callback();
	    }
	);

	// socket.on("disconnect") - socket.io event for when the client disconnects
	socket.on("disconnect", function () {
		output = "Client disconnected (" + socket.request.connection.remoteAddress + ")";   	
		io.sockets.emit("serverSendMessage", output);
	 	console.log(output);
	  	streamLog.write(output + "\r\n");
		updateMessageHistory(output);			      	
	});
});





// getCurrentDate() - Returns a string containing the current date/time in the format YYYY-MM-DD HH:MMM:SS 
function getCurrentDate() {
	var currentDate = new Date();
	var day = (currentDate.getDate() < 10 ? '0' : '') + currentDate.getDate();
	var month = ((currentDate.getMonth() + 1) < 10 ? '0' : '') + (currentDate.getMonth() + 1);
	var year = currentDate.getFullYear();
	var hour = (currentDate.getHours() < 10 ? '0' : '') + currentDate.getHours();
	var minute = (currentDate.getMinutes() < 10 ? '0' : '') + currentDate.getMinutes();
	var second = (currentDate.getSeconds() < 10 ? '0' : '') + currentDate.getSeconds();

	return(year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second);
}


// getLogFilename() - Returns the file path for logging to a file
function getLogFilename() {
	var currentDate = new Date();
	var day = (currentDate.getDate() < 10 ? '0' : '') + currentDate.getDate();
	var month = ((currentDate.getMonth() + 1) < 10 ? '0' : '') + (currentDate.getMonth() + 1);
	var year = currentDate.getFullYear();
	var hour = (currentDate.getHours() < 10 ? '0' : '') + currentDate.getHours();
	var minute = (currentDate.getMinutes() < 10 ? '0' : '') + currentDate.getMinutes();
	var second = (currentDate.getSeconds() < 10 ? '0' : '') + currentDate.getSeconds();

	return("./logs/" + year + month + day + "_" + hour + minute + second + ".txt");
}


// updateMessageHistory() - Updates the message history log
function updateMessageHistory(msg) {
	// The message history log is a simple queue, push the latest message to the end
	msgLog.push(msg);	

	// Prune history log by removing the first message from the queue
	if(msgLog.length > maxMsgLogLength) {
		msgLog.shift();
	}
}








