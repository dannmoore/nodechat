# NodeChat

### Overview:

NodeChat is a simple client/server chat web application written in Javascript for Node.js.



### Setup:

Installation and setup of Node.js is beyond the scope of this document.  You will require Node.js as well as Socket.io on the server.  Socket.io can be installed within Node.js by using the command "npm install socket.io".  A sample certificate and private key has been provided in the "keys" directory, but it is advisable to generate your own for security.



### Usage:

To launch the server, use the command "node.exe nodechat.js".  Then use a browser to visit "nodechat.html" on the server.  For example, https://127.0.0.1:3000/nodechat.html (The default port for the server is 3000)

<img src="https://gitlab.com/dannmoore/nodechat/-/raw/master/media/screenshot.png" width="640"/>

On the client web page is a simple layout.  At the top you may change your username.  The middle section contains the chat text.  At the bottom is an input where you may type a message.  Finally, an emoji may be inserted into the message by clicking on the picture icon to the lower-right of the page.



### Notes:

- More emojis can be added by modifying the "imagedict" in nodechat.js and imagepopup.js.
- The default port of 3000 can be changed by modifying app.listen() in nodechat.js



### Legal:
Copyright (C) 2024 Dann Moore.  All rights reserved.





