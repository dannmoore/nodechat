// Flag for whether the image popup is visible or not
var bImagePopup = false;

// Create an image dictionary to translate tokens into images
var imagedict = {};
imagedict[1] = {text: ":thumbsup:", src: "/img/thumbsup.png"};
imagedict[2] = {text: ":smile:", src: "/img/smile.png"};
imagedict[3] = {text: ":wink:", src: "/img/wink.png"};
imagedict[4] = {text: ":grin:", src: "/img/grin.png"};
imagedict[5] = {text: ":laugh:", src: "/img/laugh.png"};
imagedict[6] = {text: ":angry:", src: "/img/angry.png"};
imagedict[7] = {text: ":shocked:", src: "/img/shocked.png"};
imagedict[8] = {text: ":omg:", src: "/img/omg.png"};
imagedict[9] = {text: ":sick:", src: "/img/sick.png"};
imagedict[10] = {text: ":poo:", src: "/img/poo.png"};
imagedict[11] = {text: ":tongue:", src: "/img/tongue.png"};
imagedict[12] = {text: ":noemotion:", src: "/img/noemotion.png"};

       

// ToggleImagePopup() - Toggles visibility of the image popup
function ToggleImagePopup() {
    if(bImagePopup) {
        $("#ImagePopup").css("display", "none");
        bImagePopup = false;
    }
    else {
        $("#ImagePopup").css("display", "block");
        bImagePopup = true;
    }
}

// CloseImagePopup() - Closes the image popup
function CloseImagePopup() {
    $("#ImagePopup").css("display", "none");    
    bImagePopup = false;
}


// GetImageCount() - Returns the number of elements in our image dictionary
function GetImageCount() {
    let len = 0;
    for(let i in imagedict) {
        len++;
    }

    return(len);
}


function GetImageToken(index) {
    return(imagedict[index].text);
}

function GetImageSrc(index) {
    return(imagedict[index].src);
}


// GetImageSrcFromToken() - Returns imagedict.src for the given token
function GetImageSrcFromToken(token) {
    let len = GetImageCount();

    for(let i = 1; i <= len; i++) {
        if(GetImageToken(i) == token) {
            return(GetImageSrc(i));
        }
    }

    return("");
}


// SelectImage() - Called when the user clicks on an image in the image popup
function SelectImage(index) {
    // index 0 is special and represents the "none" selection
    if(index != 0) {
        $("form#chat #msg_text").val($("form#chat #msg_text").val() + GetImageToken(index));
    }

    $("form#chat #msg_text").focus();
    ToggleImagePopup();
}

// SetupPopupImages() - Sets up the div containing emoji images
function SetupPopupImages() {
    let len = GetImageCount();

    // index 0 is special and represents the "none" selection
    $("#img0").attr("title", "none");

    // Loop through imagedict and create each image in the div
    for(let i = 1; i <= len; i++) {
        $("#ImagePopup").append("<img id=\"img" + i + "\" width=32px>");    

        $("#img" + i).attr("src", GetImageSrc(i));
        $("#img" + i).attr("title", GetImageToken(i));

        $("#img" + i).on("click", function() {
             SelectImage(i);
             return(false);
        });
    }
}


// ReplaceTokensWithImages() - Replaces tokens in the given message with image tags
function ReplaceTokensWithImages(msg) {
    let len = GetImageCount();

    // Loop through imagedict and replace tokens with the appropriate image
    for(let i = 1; i <= len; i++) {   
        msg = msg.split(GetImageToken(i)).join("<img src=\"" + GetImageSrc(i) + "\" title=\"" + GetImageToken(i) + "\">");
    }

    return(msg);
}